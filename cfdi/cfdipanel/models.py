from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from decimal import Decimal

# Model of User
class UserProfile(models.Model):
    user = models.OneToOneField(User, primary_key=True)
    avatar = models.ImageField('profile picture', upload_to='static/logos/',
                               null=True, blank=True)
    karma = models.IntegerField(default=0, blank=True)

    def set_avatar(self):
        _avatar = self.avatar
        if not _avatar:
            self.avatar = "static/logos/default.png"

    def __str__(self):
        return self.user.username

# Model of Reports
class Report(models.Model):
    owner = models.ForeignKey(UserProfile)
    title = models.CharField(max_length=40)
    body = models.TextField()
    created_date = models.DateTimeField(
        default=timezone.now)
    published_date = models.DateTimeField(
        auto_now=True)

    #def publish(self):
        #self.published_date = timezone.now()
        #self.save()

    def __str__(self):
        return self.title

# Model of Facturas
class Invoice(models.Model):
    owner = models.ForeignKey(UserProfile)
    report = models.ForeignKey(Report)
    uuid = models.CharField(max_length=36)
    emision_date = models.CharField(max_length=28)
    re = models.CharField(max_length=13)
    rr = models.CharField(max_length=13)
    type_invoice = models.CharField(max_length=10)
    status = models.CharField(max_length=16, blank=True)
    tt = models.DecimalField(max_digits=19, decimal_places=9,
                                default=Decimal(0))
    def __str__(self):
        return self.uuid
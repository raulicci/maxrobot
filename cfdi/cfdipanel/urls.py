from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.contrib.auth.views import login
from django.contrib.auth.views import logout
from django.core.urlresolvers import reverse
from . import views


urlpatterns = [
    url(r'^$', views.index),
    url(r'^escritorio$', views.report_list),
    url(r'^report/(?P<pk>[0-9]+)/$', views.report_detailh, name='report_detailh'),
    url(r'^login$', auth_views.login, {'template_name':
         'cfdipanel/login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/login'},
         name='logout'),
    url(r'^admin/', admin.site.urls),
]
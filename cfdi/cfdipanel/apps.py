from django.apps import AppConfig


class CfdipanelConfig(AppConfig):
    name = 'cfdipanel'

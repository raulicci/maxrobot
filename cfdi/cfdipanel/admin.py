from django.contrib import admin
from .models import UserProfile, Report, Invoice

class UserProfileAdmin(admin.ModelAdmin):
    pass

class InvoiceInline(admin.TabularInline):
    model = Invoice
    extra = 3

class ReportAdmin(admin.ModelAdmin):
    inlines = [InvoiceInline, ]

admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Report, ReportAdmin)
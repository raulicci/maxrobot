from django.db import models
from django.shortcuts import render, reverse, get_object_or_404
from django.views.decorators.csrf import csrf_protect
from django.utils import timezone
from . import views
import cfdipanel.models
from cfdipanel import models
from .models import Report, Invoice

# Create your views here.
def index(request):
    return render(request, 'cfdipanel/index.html', {})

def report_list(request):
    reports = Report.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    return render(request, 'cfdipanel/report_list.html', {'reports': reports})

def login(request):
    return render(request, reverse("login"))

def report_detailh(request, pk):
        report = get_object_or_404(Report, pk=pk)
        invoices = Invoice.objects.all().filter(report=report.id)
        return render(request, 'cfdipanel/report_detailh.html', {'report': report,'invoices': invoices})
'''
@login_required
def avatar(request):
    user_profile = request.user.get_profile()
    avatar = user_profile.avatar
'''
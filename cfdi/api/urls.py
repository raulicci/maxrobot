from django.conf.urls import include, url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views


report_list = views.ReportViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

report_detail = views.ReportViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

invoice_creation = views.ReportViewSet.as_view({
    'post': 'set_invoice'
})


urlpatterns = [
    url(r'^v1/reports/$', report_list, name='report_list'),
    url(r'^v1/report/(?P<pk>[0-9]+)/$', report_detail, name='report_detail'),
    url(r'^v1/report/(?P<pk>[0-9]+)/invoice/$', invoice_creation,
        name='invoice_creation'),
]
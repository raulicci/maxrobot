from django.shortcuts import render
from rest_framework import viewsets, status, serializers
from rest_framework.response import Response
from rest_framework.decorators import detail_route
import base64
from suds.client import Client
from . import views

# Create your views here.

from cfdipanel.models import Report, Invoice, UserProfile
from cfdi.serializers import ReportSerializer, InvoiceSerializer, UserProfileSerializer

class ReportViewSet(viewsets.ModelViewSet):
    queryset = Report.objects.all()
    serializer_class = ReportSerializer

    @detail_route(methods=['post'])

    def set_invoice(self, request, pk=None):
# Recovery data POST fields for send api SAT
        posti = request.data
        uuid = posti['uuid']
        re = posti['re']
        rr = posti['rr']
        tt = posti['tt']
        url = 'https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc?wsdl'
        client = Client(url)
        string = "?re=" + re + "&rr=" + rr + \
                 "&tt=" + tt + "&id=" + uuid
        response = client.service.Consulta(string)

# Create content for field status
        estado = response.Estado
        posti['status'] = estado
        # Get report object
        my_report = self.get_object()
        serializer = InvoiceSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(report=my_report)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
from rest_framework import serializers
from cfdipanel.models import Report, Invoice, UserProfile

class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ('user', 'karma')


class InvoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invoice
        fields = ('uuid', 're', 'rr', 'tt',
                  'emision_date', 'type_invoice', 'status', 'owner')


class ReportSerializer(serializers.ModelSerializer):
    owner = UserProfileSerializer(read_only=True)
    ownerId = serializers.PrimaryKeyRelatedField(write_only=True,
                                                 queryset=UserProfile.objects.all(),
                                                 source='owner')
    invoices = InvoiceSerializer(many=True,
                                 read_only=True,
                                 source='invoice_set')

    class Meta:
        model = Report
        fields = ('id', 'title', 'body', 'owner', 'ownerId', 'invoices')
Django==1.10.4
django-bootstrap-themes==3.3.6
djangorestframework==3.5.3
olefile==0.43
Pillow==4.0.0
